const https = require('https')
const http = require('http')
const app = require('./app')
const fs = require('fs')

const port = process.env.PORT || 5000

if ('production' === process.env.ENV) {
  https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/reddit.qnzl.co/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/reddit.qnzl.co/cert.pem')
  }, app).listen(port, () => {
    console.log(`Express server listening on port ${port}, ENV=production`)
  })
} else {
  http.createServer(app).listen(3075, () => {
    console.log(`Express server listening on port 3075, ENV=staging`)
  })
}
