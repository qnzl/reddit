const compression = require('compression')
const bodyParser = require('body-parser')
const ecstatic = require('ecstatic')
const express = require('express')
const session = require('express-session')
const minify = require('mollify')
const path = require('path')

const app = express()
const router = express.Router()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(minify({
    dir: `${__dirname}/../public`,
    is: true
}))
app.use(compression())
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: true
}))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type')

  next()
})

router.use('/api', require('./routes/index'))

app.use(router)

app.use(ecstatic({
  root: `${__dirname}/../public`,
  cache: 'max-age=86400'
}))

// All other routes
router.use('*', (req, res, next) => {
  if (req.baseUrl.indexOf('.') > -1) {
    return next()
  }

  // What routes to respond with index.html with
  const publicRoutes = [
    new RegExp('/dashboard'),
    new RegExp('/redirect'),
    new RegExp('/r/.*'),
  ]

  const match = publicRoutes.some(route => route.test(req.baseUrl))

  if (match) {
    return res.sendFile(path.join(__dirname, '../public', 'index.html'))
  }

  return next()
})

app.use((err, req, res, next) => {
  if (err) {
    console.error('Error occurred: ', err)
    res.statusCode = 500
    return res.send(err)
  }

  res.end()
  return next()
})

module.exports = app
