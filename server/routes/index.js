const { unescape } = require('true-html-escape')
const mongoose = require('mongoose')
const express = require('express')
const Reddit = require('snoowrap')
const debug = require('debug')('qnzl:reddit:user')
const pify = require('pify')
const html = require('choo/html')

const reddit = new Reddit({
  userAgent: 'Qnzl Getter https://reddit.qnzl.co',
  clientId: '4_nLkHF-NjYZWA',
  clientSecret: process.env.CLIENT_SECRET,
  refreshToken: process.env.REFRESH_TOKEN
})

const cache = {}
const router = express.Router()

router.get('/posts/:subreddit', async (req, res, next) => {
  const handleResponse = (err, data) => {
    if (err) {
      console.error(`Error occurred while getting posts ${req.params.subreddit}`, err)

      return res.sendStatus(500)
    }

    return res.json({ posts: data })
  }

  const retrieveUntilMax = ({
    name,
    posting,
    amount = 25,
    limit = 100,
    wait = 10000,
    postProcessFn = () => {},
  }) => {
    // Stop retrieving
    if (posting.length > limit) {
      debug(`${name} is maxed out with ${limit} items`)
      return
    } else {
      // Recursively retrieve more posts.
      // The timeout is used to offset queries so
      // we don't flood the API and use up our usage limit.
      setTimeout(async () => {
        const items = await posting.fetchMore({ amount })

        debug(`${name} now has ${items.length} items`)
        cache[name] = postProcessFn(items)

        return retrieveUntilMax({ name, posting: items, amount, limit, wait, postProcessFn })
      }, wait)
    }
  }

  const cacheTag = `${req.params.subreddit}.posts`

  if (cacheTag in cache) {
    return handleResponse(null, cache[cacheTag])
  } else {
    try {
      let posts = await reddit
        .getSubreddit(req.params.subreddit)
        .getHot({ limit: 25 })

      const cleanedPosts = posts.map(({
        thumbnail,
        id,
        title,
        thumbnail_height,
        thumbnail_width,
        url,
      }) => {
        return {
          thumbnail,
          thumbnail_width,
          thumbnail_height,
          id,
          title,
          url,
        }
      })

      cache[cacheTag] = cleanedPosts
      setTimeout(() => {
        delete cache[cacheTag]
      }, 90000)

      retrieveUntilMax({
        name: cacheTag,
        posting: posts,
        postProcessFn: (posts) => {
          return posts.map(({
            thumbnail,
            id,
            title,
            thumbnail_height,
            thumbnail_width,
            url,
          }) => {
            return {
              thumbnail,
              thumbnail_width,
              thumbnail_height,
              id,
              title,
              url,
            }
          })
        }
      })

      return handleResponse(null, cleanedPosts)
    } catch (e) {
      return handleResponse(e)
    }
  }
})

router.get('/posts/:subreddit/:postId', async (req, res, next) => {
  const cacheTag = `${req.params.subreddit}.posts.${req.params.postId}`

  if (cacheTag in cache) {
    return res.json(cache[cacheTag])
  } else {
    let submission

    try {
      submission = await reddit.getSubmission(req.params.postId).fetch()
    } catch (e) {
      console.error(e)
    }

    let {
      thumbnail,
      thumbnail_width,
      thumbnail_height,
      id,
      title,
      url,
      comments,
      selftext_html: body,
    } = submission

    const parseComment = (comment) => {
      let parsedReplies = []

      if (comment.replies.length > 0) {
        parsedReplies = comment.replies.map(parseComment)
      }

      const { ups, author, body_html: body } = comment
      const regex = /(?<=\<p\>).*(?=\<\/p\>)/g
      let lines = body.match(regex)

      if (!lines) {
        lines = [ body ]
      }

      const cleanLine = (line) => {
        // Yes, the browser complains about this.
        return line.replace(`<br/>`, `</br>`)
      }

      lines = lines.map(unescape).map(cleanLine)
      return {
        ups,
        author,
        body: lines,
        replies: parsedReplies
      }
    }

    title = unescape(title)

    comments = comments.map(parseComment)

    cache[cacheTag] = {
      comments,
      post: {
        thumbnail,
        thumbnail_width,
        body: body ? unescape(body) : '',
        thumbnail_height,
        id,
        title,
        url,
      }
    }

    setTimeout(() => {
      delete cache[cacheTag]
    }, 1800000)

    return res.json(cache[cacheTag])
  }
})

router.post('/search', async (req, res, next) => {
  try {
    const nsfwTag = (nsfw) => {
      return nsfw ? `nsfw` : `not-nsfw`
    }

    const cacheTag = `${nsfwTag(req.body.includeNsfw)}-${req.body.subreddit}`

    debug(`checking cache for ${cacheTag}`)
    if (cacheTag in cache) {
      return res.json({ subreddits: cache[cacheTag] })
    } else {
      let subreddits = await reddit.searchSubreddits({
        query: req.body.subreddit,
        limit: 15,
      })

      cache[cacheTag] = subreddits

      setTimeout(() => {
        delete cache[cacheTag]
      }, 720000)

      return res.json({ subreddits })
    }
  } catch (error) {
    debug(`error occurred:`, error)

    return res.sendStatus(500)
  }
})

// Used for getting refresh token to use the API.
// End users don't need to currently use this.
router.get('/redirect', async (req, res, next) => {
  const { code } = req.query

  Reddit.fromAuthCode({
    code,
    userAgent: 'Qnzl Getter https://reddit.qnzl.co',
    clientId: '4_nLkHF-NjYZWA',
    clientSecret: process.env.CLIENT_SECRET,
    redirectUri: 'http://localhost:3075/api/redirect'
  }).then(async r => {
    req.session.reddit = r
    await pify(req.session.save)()

    return res.sendStatus(200)
  })
})

module.exports = router
