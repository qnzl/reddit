const loading = require('mindful-loading')
const html = require('choo/html')

module.exports = (app) => {
  app.use((state, emitter) => {
    emitter.on('navigate', () => {
      state.err = ''
      state.comments = null
      state.posts = null
    })

    state.loading = html`
      <div class="pa-2 h-screen grid grid-col-3 grid-row-3 justify-items-center bg-gray-900 text-gray-500 text-4xl">
        <div class="col-start-1 row-start-2 text-center">
          ${loading()}
        </div>
      </div>
    `
  })
}
