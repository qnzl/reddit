const landing = require('../views/landing')
const subreddit = require('../views/subreddit')
const post = require('../views/post')


module.exports = (app) => {
  app.route('/', landing)
  app.route('/redirect', landing)
  app.route('/r/:subreddit', subreddit)
  app.route('/r/:subreddit/comments/:postId', post)
  app.route('/r/:subreddit/comments/:postId/:title', post)
}
