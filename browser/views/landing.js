const getFormData = require('get-form-data')
const request = require('superagent')
const Reddit = require('snoowrap')
const html = require('choo/html')

module.exports = (state, emit) => {
  const search = (e) => {
    e.preventDefault()

    const data = getFormData(e.target)

    const elem = document.getElementById('searchBtn')
    elem.innerText = ''
    elem.classList.add('loading')

    state.includeNsfw = data.includeNsfw === 'on'

    request.post('/api/search')
      .send(data)
      .end((err, resp) => {
        if (err) {
          state.err = err

          return emit('render')
        }

        state.err = ''
        state.subreddits = resp.body.subreddits

        return emit('render')
      })
  }


  return html`
    <div class="pl-3 bg-gray-900 h-100 text-gray-500">
      <header class="text-2xl text-gray-400">
        minimalism for reddit
      </header>
      <section name="posts" id="posts" class="mt-10 mb-5">
        <div class="grid grid-cols-3 grid-rows-17 justify-items-center items-end">
          <h3 class="mb-10 col-start-2 row-start-1 text-xl">Search for subreddit</h3>
          <form onsubmit=${search} class="grid grid-cols-3 grid-rows-3 row-start-2 col-span-3 justify-items-center">
            <input type="text" name="subreddit" class="col-span-3 border-2 border-gray-600 row-start-1 mb-4 bg-gray-700 text-gray-400">
            <button class="col-start-2 row-start-2 w-20 ph-3 pv-2 input-reset border-2 border-gray-600 bg-transparent text-lg rounded-md mb-1" id="searchBtn" type="submit">
              Search
            </button>
            <span class="pl-2 mt-1 col-start-2 row-start-3">
              <input type="checkbox" name="includeNsfw" class="mr-1 bg-gray-700 border-2 border-gray-600" />
              <label>Include NSFW</label>
            </span>
            ${(() => {
              return html`
                <div class="red mt-2">
                  ${state.err}
                </div>
              `
            })()}
          </form>
          ${(() => {
            if (state.subreddits) {
              return state.subreddits.map((subreddit) => {
                if (!state.includeNsfw && subreddit.over18) {
                  return null
                }

                return html`
                  <div class="col-start-2">
                    <a href="/r/${subreddit.display_name}">${subreddit.display_name}</a>
                  </div>
                `
              })

              state.includeNsfw = false
            }
          })()}
        </div>
      </section>
      <div class="text-gray-400 fixed-ns relative flex justify-center f7 pa-2-ns pa-2" style="right:0;bottom:0">
        Created by <a rel="noreferrer" href="https://twitter.com/madamelic" class="pl-1" target="_blank">@madamelic</a>
      </div>
    </div>
  `
}
