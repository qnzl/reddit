const html = require('choo/html')

module.exports = (state, emit) => {
  return html`
    <div>
      <div class="pa4 flex justify-center black-80">
        <p>
          <div class="f4 b mb2">Your data is your data.</div>
          
        </p>
      </div>
    </div>
  `
}