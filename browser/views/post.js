const getFormData = require('get-form-data')
const request = require('superagent')
const Reddit = require('snoowrap')
const html = require('choo/html')

module.exports = (state, emit) => {
  if (!state.comments) {
    request(`/api/posts/${state.params.subreddit}/${state.params.postId}`)
      .end((err, resp) => {
        if (err) {
          state.err = err

          return emit('render')
        }

        state.err = ''
        state.post = resp.body.post
        state.comments = resp.body.comments

        return emit('render')
      })

    return state.loading
  }

  if (state.err) {
    return html`
      <div class="red">
        ${state.err} 😔
      </div>
    `
  }

  const back = (e) => {
    return emit('pushState', `/r/${state.params.subreddit}`)
  }

  return html`
    <div class="pl-3 h-100 bg-gray-900">
      <header class="flex items-center pb-2 mb-10 border-b-2 border-gray-400">
        <img
          src="/img/arrow-left-gray.png"
          class="w-5 h-5 mr-3"
          onclick=${back}
        />
        <span class="text-2xl text-gray-400">
          /r/${state.params.subreddit}
        </span>
      </header>
      <section name="posts" id="posts" class="mt-3">
        <div class="flex mb-3 items-center">
          <a href="${state.post.url}">
            <img
              class="mr-2"
              src="${state.post.thumbnail !== 'self' ? state.post.thumbnail : '/img/blank.png'}"
              height="${state.post.thumbnail_height || 105}"
              width="${state.post.thumbnail_width || 140}"
            />
          </a>
          <h3>
            <a class="text-gray-400 text-lg font-bold" href="${state.post.url}">${state.post.title}</a>
          </h3>
        </div>
        ${(() => {
          if (state.post.body) {
            return html`
              <div class="mb-3">
                ${(() => {
                  return html([ '<div class="text-gray-500">', state.post.body, '</div>' ])
                })()}
              </div>
            `
          }
        })()}
        <div class="ml-1 ml-4-ns mr-3">
          ${(() => {
            const buildTree = (comment, level = 0) => {
              const threadColors = [
                'border-red-600',
                'border-yellow-600',
                'border-yellow-300',
                'border-green-600',
                'border-blue-600',
                'border-indigo-600',
                'border-purple-600',
              ]

              return html`
                <div class="grid border-l-2 ${threadColors[level % threadColors.length]}">
                  <div class="pa-2 ml-2 mt-1 flex w-100">
                    <div class="ml-2">
                      <div class="text-gray-500 text-sm">
                        ${comment.author.name}
                      </div>
                      ${(() => {
                        return comment.body.map((body, index) => {
                          return html([`<p ${index !== comment.body.length && 'class="pb-2 text-gray-400"'}>`, body, '</p>' ])
                        })
                      })()}
                    </div>
                  </div>
                  <div class="pa-2 ml-4 mt-1">
                    ${comment.replies.map((r) => buildTree(r, level+1))}
                  </div>
                </div>
              `
            }

            const comments = state.comments.map((comment) => {
              return html`
                <div class="ba b--black-10 pa-3 br-3 mb-3">
                  ${buildTree(comment, 0)}
                </div>
              `
            })

            return comments
          })()}
        </div>
      </section>
      <div class="text-gray-400 fixed-ns relative flex justify-center text-sm pa-2-ns pa-2" style="right:0;bottom:0">
        Created by <a rel="noreferrer" href="https://twitter.com/madamelic" class="pl-1" target="_blank">@madamelic</a>
      </div>
    </div>
  `
}
