const getFormData = require('get-form-data')
const request = require('superagent')
const loading = require('mindful-loading')
const Reddit = require('snoowrap')
const html = require('choo/html')

module.exports = (state, emit) => {
  if (!state.posts) {
    request(`/api/posts/${state.params.subreddit}`)
      .end((err, resp) => {
        if (err) {
          state.err = err

          return emit('render')
        }

        state.err = ''
        state.posts = resp.body.posts

        return emit('render')
      })

    return state.loading
  }

  if (state.err) {
    return html`
      <div class="red">
        ${state.err} 😔
      </div>
    `
  }

  const openPost = (postId) => {
    return (e) => {
      e.stopPropagation()

      return emit(
        'pushState',
        `/r/${state.params.subreddit}/comments/${postId}`,
      )
    }
  }

  const openLink = (postUrl) => {
    return (e) => {
      e.stopPropagation()

      window.open(postUrl, "_self")
    }
  }

  const back = (e) => {
    return emit('pushState', '/')
  }

  return html`
    <div class="pl-3 h-100 bg-gray-900">
      <header class="flex items-center pb-2 mb-10 border-b-2 border-gray-400">
        <img
          src="/img/arrow-left-gray.png"
          class="w-5 h-5 mr-3"
          onclick=${back}
        />
        <span class="text-2xl text-gray-400">
          /r/${state.params.subreddit}
        </span>
      </header>
      <section name="posts" id="posts" class="mt-3">
        <ul class="divide-y divide-gray-800">
          ${(() => {
            return state.posts.map((post) => {
              return html`
                <li class="py-8" onclick=${openPost(post.id)}>
                  <div class="flex space-x-3">
                    <img
                      class="rounded-md"
                      height="${post.thumbnail_height || 105}"
                      width=${post.thumbnail_width || 140}
                      src="${post.thumbnail !== 'self' ? post.thumbnail : '/img/blank.png'}"
                      onclick=${openLink(post.url)}
                    />
                    <div class="flex-1 space-y-1">
                      <div class="flex items-center justify-between">
                        <span class="text-lg font-bold text-gray-400">${post.title}</span>
                      </div>
                    </div>
                  </div>
                </li>
              `
            })
          })()}
        </ul>
      </section>
      <div class="text-gray-400 fixed-ns relative flex justify-center text-sm pa2-ns pa2" style="right:0;bottom:0">
        Created by <a rel="noreferrer" href="https://twitter.com/madamelic" class="pl-1" target="_blank">@madamelic</a>
      </div>
    </div>
  `
}
