const choo = require('choo')

const app = choo()

require('./lib/router')(app)
require('./lib/global')(app)
require('./lib/track')(app)

document.body.appendChild(app.start())
